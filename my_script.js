(function ($) {
	Drupal.behaviors.p_18 = {
		attach: function () {
			 $('.apply-ajax').click(function(event) {
			 	event.preventDefault();

				var number = $('.node_title').length;
				var result = $(document.createElement('div'));
				$(result).html('Title(s) on this page shown ' + number);

				$(result).dialog({
					"body": $(result).html(),
    			"title": "",
    			"show": true
				});

			});
		}
	}
})(jQuery);
